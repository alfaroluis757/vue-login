import Login from './components/Auth/Login.vue';
import App from './components/Auth/App.vue';
import Register from './components/Auth/Register.vue';
import Forgot from './components/Auth/forgot.vue';
import Reset from './components/Auth/reset.vue';
import NotFound from './components/Auth/NotFound.vue';
import Example from './components/Admin/ExampleComponent.vue';



export const routes=[
    {
        path: '/',
        name: 'login',
        component: Login,
        meta: {
            auth: false,
        }
    }, 
    //prueba
    {
        path: '/login',
        name: 'loginn',
        component: Login,
        meta: {
            auth: false,
        }
    },{
        path: '/forgot',
        name: 'forgot',
        component: Forgot,
        meta: {
            auth: false,
        }
    },{
        path: '/404',
        name: '404',
        component: NotFound,
       
    },
    {
        path: '/Example',
        name: 'Example',
        component: Example,
        meta: {
            auth: true,
        }
    }
]